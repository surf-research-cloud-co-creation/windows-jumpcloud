# Windows Jumpcloud


SURF Research cloud plugin for installing and configuring Jumpcloud.

## Pre-requisites:

* Add after the rsc-external-windows plugin in a Research Cloud product template
* JC_API_KEY plugin parameter containing the API key to connect to jumpcloud
