# Adapted from https://raw.githubusercontent.com/TheJumpCloud/support/master/scripts/windows/InstallWindowsAgent.ps1 on 10/07/2023

$LOGFILE = "c:\logs\windows-jumpcloud.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}



$AGENT_PATH = "${env:ProgramFiles}\JumpCloud"
$AGENT_BINARY_NAME =  "jumpcloud-agent.exe" #"jcagent-msi-signed.msi"
$AGENT_INSTALLER_URL = "https://cdn02.jumpcloud.com/production/jcagent-msi-signed.msi"
$AGENT_INSTALLER_PATH = "C:\Windows\Temp\jcagent-msi-signed.msi"


Function AgentIsOnFileSystem() {
    Test-Path -Path:(${AGENT_PATH} + '/' + ${AGENT_BINARY_NAME})
}

Function InstallAgent() {

    $params = @{
        "FilePath" = "msiexec.exe"
        "ArgumentList" = @(
            "/i"
            $AGENT_INSTALLER_PATH
            "/quiet"
            "/lv"
            "C:\Windows\Temp\jcUpdate.log"
            "JCINSTALLERARGUMENTS=`"-k $JumpCloudConnectKey /VERYSILENT /NORESTART /NOCLOSEAPPLICATIONS`""
        )
        "Verb" = "runas"
        "PassThru" = $true
    }

    $installer = Start-Process @params
    $installer.WaitForExit()

    If ($installer.ExitCode -ne 0) {
        Write-Log 'JumpCloud Agent installation failed, installer process exited with non-zero exit code'
        Exit $installer.ExitCode
    }

}

Function DownloadAgentInstaller() {
    (New-Object System.Net.WebClient).DownloadFile("${AGENT_INSTALLER_URL}", "${AGENT_INSTALLER_PATH}")
}

Function DownloadLink($Link, $Path) {
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    $WebClient = New-Object -TypeName:('System.Net.WebClient')
    $WebClient.DownloadFile("$Link", "$Path")
    $WebClient.Dispose()
}


Function DownloadAndInstallAgent() {

    If (!(AgentIsOnFileSystem)) {
        Write-Log 'Downloading JCAgent Installer'
        DownloadAgentInstaller
        Write-Log 'JumpCloud Agent Download Complete'
        Write-Log 'Running JCAgent Installer'
        InstallAgent
        

        If (AgentIsOnFileSystem) {
          Write-Log 'JumpCloud Agent installation completed'
        } Else {
          Write-Log'JumpCloud Agent installation failed, agent not found on file system'
            Exit 1
        }

    } else {
      Write-Log "JumpCloud Agent already installed!"
    }
}


Function Main {

  Write-Log "Start windows-jumpcloud"

  try {
    $JumpCloudConnectKey = [System.Environment]::GetEnvironmentVariable('JC_API_KEY')

    DownloadAndInstallAgent

  }
  catch {
      Write-Log "$_"
      Throw $_
  }
  Write-Log "End windows-jumpcloud"
 
}

Main    